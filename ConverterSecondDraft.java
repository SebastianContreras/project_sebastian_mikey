public class Converter {
//Your names go here:
/*
* @Author: Michael Vassilev
*		   Sebastian Contreras
* 
*
*/
	private double celsiusToFahrenheit(double C){
	double F = (C * (9/5) + 32);
	return F;
	}
	private double fahrenheitToCelsius(double F){
	double C = ((F - 32) * (5/9));
	return C;
	}
public static void main(String[] args) {
//TODO: The first student will implement this method.
// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
	
	double tempFahrenheit = celsiusToFahrenheit(180);
	double tempCelsius =  fahrenheitToCelsius(250);
}
}